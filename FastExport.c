#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include "FileHandler.h"
#include "FastExport.h"

#define BLOB_LEN 2000
#define SHOW_PROGRESS_EACH 100

#define BLOBS_PER_COMMIT 3
#define DIR_LEVELS 2
#define COMMITS_PER_DIR 10
#ifdef _WIN64

#include <io.h>

#endif

typedef struct export_file export_file;

char *author = "Pawel Sucholbiak <pawel.sucholbiak@gmail.com>";
char *commit_timestamp = "1346450400 +0200";

void print_blob(size_t id, int length);

void print_commit(size_t commit_no, size_t mark_id, size_t parent_id, export_file *files, int file_count);

void export_file_free(export_file *files, int count);

void print_commit_message(size_t commit_no);

void run_fast_export(int end) {
#ifdef _WIN64
    // setting binary output (\n only) otherwise `git fast-import` complains about \r\n
    _setmode(_fileno(stdout), _O_BINARY);
#endif
    size_t mark_id = 1;
    size_t path_index = 0;
    size_t parent_id = 0;
    for (size_t commit_no = 1; commit_no <= end; commit_no++) {
        if (commit_no % COMMITS_PER_DIR == 0) {
            path_index++;
        }
        export_file *files = calloc(BLOBS_PER_COMMIT, sizeof(export_file));
        for (int blob_counter = 0; blob_counter < BLOBS_PER_COMMIT; blob_counter++) {
            files[blob_counter].mark_id = mark_id;
            char *filename = get_filename_by_index(mark_id);
            char *path = get_path_by_index(path_index, DIR_LEVELS);
            files[blob_counter].filepath = join_paths(path, filename);
            print_blob(mark_id, BLOB_LEN);
            mark_id++;
            free(filename);
            free(path);
        }

        print_commit(commit_no, mark_id, parent_id, files, BLOBS_PER_COMMIT);
        parent_id = mark_id;
        mark_id++;
        export_file_free(files, BLOBS_PER_COMMIT);
    }
}

void export_file_free(export_file *files, int count) {
    for (int i = 0; i < count; i++) {
        free(files[i].filepath);
    }
    free(files);
}

void print_blob(size_t id, int length) {
    printf("blob\nmark :%zu\ndata %d\n", id, length + 1);
    char *content = get_random_string(length, 1);
    printf("%s\n", content);
    free(content);
    printf("\n");
}

void print_commit(size_t commit_no, size_t mark_id, size_t parent_id, export_file *files, int file_count) {
    printf("commit refs/heads/master\nmark :%zu\n", mark_id);
    printf("author %s %s\n", author, commit_timestamp);
    printf("committer %s %s\n", author, commit_timestamp);
    print_commit_message(commit_no);

    if (parent_id != 0) {
        printf("from :%zu\n", parent_id);
    }
    for (int i = 0; i < file_count; i++) {
        printf("M 100644 :%zu %s\n", files[i].mark_id, files[i].filepath);
    }
    printf("\n");
}
void print_commit_message(size_t commit_no) {
    char commit_msg[50];
    snprintf(commit_msg, sizeof(commit_msg), "commit no: %zu", commit_no);
    printf("data %llu\n%s\n", strlen(commit_msg) + 1, commit_msg);
};