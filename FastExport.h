#ifndef REPO_GENERATOR_FASTEXPORT_H
#define REPO_GENERATOR_FASTEXPORT_H
typedef struct export_file {
    size_t mark_id;
    char *filepath;
} export_file;

void run_fast_export(int end);

#endif //REPO_GENERATOR_FASTEXPORT_H