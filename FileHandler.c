#include <stdlib.h>
#include <math.h>
#include <string.h>

#define FILE_LENGTH 2
#define DIR_LENGTH 1

int MIN_LETTER = 97;
int MAX_LETTER = 122;
char PATH_SEPARATOR = '/';
char POSSIBLE_CHARS[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.-#'?!";
char NEWLINE = '\n';

/**
* Returns a filename with static extension (.txt) based on the index. 
* Index is converted to combination of letters a-z with a length defined in FILE_LENGTH
* 
* @param int index value to be converted 
* @out char* to a filename in format [a-z]{FILE_LENGTH}\.txt
* 
* Pointer needs to be FREED
*/
char *get_filename_by_index(int index) {
    char *filename;
    char *FILE_EXT = ".txt";
    int letter_range = MAX_LETTER - MIN_LETTER;
    filename = malloc(FILE_LENGTH + sizeof(FILE_EXT));

    index %= (int) pow(letter_range, FILE_LENGTH); //resetting counter after zzz

    for (int i = FILE_LENGTH - 1; i >= 0; i--) {
        int current_index = index % letter_range;
        index = (int) index / letter_range;
        filename[i] = (char) (MIN_LETTER + current_index);
    }
    filename[FILE_LENGTH] = '\0';
    strcat(filename, FILE_EXT);

    return filename;
}

char *get_path_by_index(int index, int levels) {
    char *path;
    int letter_range = MAX_LETTER - MIN_LETTER;
    int path_length = DIR_LENGTH * 2 * levels; // DIR_LENGTH followed by slash
    path = malloc(path_length); // last slash would be replaced with \0
    index %= (int) pow(letter_range, levels); //resetting counter after zz

    for (int i = path_length - 1; i >= 0; i -= DIR_LENGTH * 2) {
        int current_index = index % letter_range;
        index = (int) index / letter_range;
        path[i] = PATH_SEPARATOR;
        path[i - 1] = (char) (MIN_LETTER + current_index);
    }
    path[path_length - 1] = '\0';
    return path;
}

char *join_paths(char *path, char *filename) {
    char *out;
    int path_length = sizeof(path);
    int filename_length = sizeof(filename);

    //+1 for separator is non needed as path will have extra \0 at the end
    int out_lenght = path_length + filename_length;
    out = malloc(out_lenght);
    int index;
    for (index = 0; index < path_length; index++) {
        if (path[index] == '\0') {
            break;
        }
        out[index] = path[index];
    }
    out[index] = PATH_SEPARATOR;
    index++;
    for (int i = 0; i < filename_length - 1; i++, index++) {
        out[index] = filename[i];
    }
    out[index] = '\0';
    return out;
}

char *get_random_string(size_t line_length, size_t line_number) {
    size_t total_len = line_length * line_number;

    if (!total_len)
        return NULL;

    char *randomString = malloc(total_len + 1);
    int possible_chars_no = (int) (sizeof(POSSIBLE_CHARS) - 1);
    int key;
    for (int x = 0; x < total_len; x++) {
        if (x % line_length == 0 && x != 0) {
            randomString[x - 1] = '\r';
            randomString[x] = '\n';
            continue;
        }
        key = rand() % possible_chars_no;
        randomString[x] = POSSIBLE_CHARS[key];
    }
    randomString[line_length] = '\0';

    return randomString;
}
