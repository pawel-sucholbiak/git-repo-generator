#pragma once

#include <stdlib.h>

char *get_filename_by_index(int index);

char *get_path_by_index(int index, int level);

char *join_paths(char *path, char *filename);

char *get_random_string(size_t line_length, size_t line_number);