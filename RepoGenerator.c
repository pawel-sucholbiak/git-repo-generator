#include <stdio.h>
#include <git2.h>
#include <stdbool.h>
#include <time.h>
#include <sys/stat.h>
#include <string.h>

#include "FileHandler.h"
#include "RepoParams.h"
#include "FastExport.h"

void error_check(int error, char *msg) {
    if (error < 0) {
        const git_error* e = giterr_last();
        printf("ERROR %s - Error %d/%d: %s\n", msg, error, e->klass, e->message);
        exit(error);
    }
}
git_commit* get_last_commit(git_repository* repo) {
    int rc;
    git_commit* commit = NULL; /* the result */
    git_oid oid_parent_commit; 

    
    int error_code = git_reference_name_to_id(&oid_parent_commit, repo, "HEAD");
    if (error_code == GIT_ENOTFOUND) {
        // no reference for HEAD, when initial commit had empty tree
        return NULL;
    }
    error_check(error_code, "resolve HEAD into a SHA1");
    error_check(git_commit_lookup(&commit, repo, &oid_parent_commit), "get the actual commit structure");
    return commit;

}

git_repository* init_and_configure_repo(char* path) {
#ifdef _WIN64
    mkdir(path);
#elif linux
    mkdir(path,0777);
#endif
    git_repository* repo = NULL;
    error_check(git_repository_init(&repo, path, 0), "repo init");
    
    return repo;
}

void get_tree_from_last_commit(git_tree** out, git_repository* repo) {
    git_commit* last_commit = get_last_commit(repo);
    git_commit_tree(out, last_commit);
}
void get_initial_tree(git_tree** out, git_repository* repo) {
    git_index* index;
    git_tree* tree;
    git_oid tree_id;

    error_check(git_repository_index(&index, repo), "open repo index");
    error_check(git_index_write_tree(&tree_id, index), "writing initial tree from index");
    
    error_check(git_tree_lookup(out, repo, &tree_id), "looking up tree by id");

    git_index_free(index);
    //return tree;
}
void create_blob(git_oid* blob_oid, git_repository* repo, const char str[]) {
    error_check(git_blob_create_frombuffer(blob_oid, repo, str, strlen(str)), "creating blob");
}

git_tree* get_root_tree(git_repository* repo) {
    git_index* index;
    git_oid tree_id;
    git_tree* tree;
    
    error_check(git_repository_index(&index, repo), "open repo index");
    error_check(git_index_write_tree(&tree_id, index), "writing initial tree from index");
    error_check(git_tree_lookup(&tree, repo, &tree_id), "looking up tree by id");
    git_index_free(index);
    
    return tree;
}
void put_tree_into_index(git_tree* tree, git_repository* repo) {
    git_index* index;
    error_check(git_repository_index(&index, repo), "open repo index");
    error_check(git_index_read_tree(index, tree), "Read a tree into the index file with stats");
    git_index_free(index);
}

git_tree* get_tree_builder_tree(git_repository *repo, const char *filename, const char *file_content) {

    git_tree* tree = NULL;

    git_treebuilder* bld = NULL;
    tree = get_root_tree(repo);
    error_check(git_treebuilder_new(&bld, repo, tree), "creating treebuilder");

    
    /*create in mem object*/
    git_oid blob_oid;
    create_blob(&blob_oid, repo, file_content);
    
    /* Add some entries */
    git_tree_entry *tree_entry;
    error_check(git_treebuilder_insert(&tree_entry, bld,
        filename,        /* filename */
        &blob_oid, //          git_object_id(obj), /* OID */
        GIT_FILEMODE_BLOB) /* mode */
        , "inserting a readme file");

    //int count = git_treebuilder_entrycount(bld);

    git_oid new_tree_id;
    error_check(git_treebuilder_write(&new_tree_id, bld), "write builder to repo tree?");
    git_treebuilder_free(bld);

    //https://stackoverflow.com/questions/57884225/how-to-commit-files-by-creating-a-memory-index-in-libgit2

    git_tree* new_tree;
    error_check(git_tree_lookup(&new_tree, repo, &new_tree_id), "new tree lookup");
    put_tree_into_index(new_tree, repo);
    
    return new_tree;
}

git_tree* get_updated_tree(git_repository* repo, const char* filename, const char* file_content) {
    git_tree* main_tree, *new_tree;
    git_oid blob_id, new_tree_id;

    main_tree = get_root_tree(repo);    
    //main_tree = get_initial_tree(repo);
    
    create_blob(&blob_id, repo, file_content);

    git_tree_update updates[] = {
        { GIT_TREE_UPDATE_UPSERT, blob_id, GIT_FILEMODE_BLOB, filename}
    };
    error_check(git_tree_create_updated(&new_tree_id, repo, main_tree, 1, updates), "creating updated tree");
    error_check(git_tree_lookup(&new_tree, repo, &new_tree_id), "new tree lookup");
    
    return new_tree;
}

git_tree* get_updated_tree_with_dir(git_repository* repo, git_tree* root_tree, const char* path, const char* filename, const char* file_content) {
    //git_tree* main_tree, * new_tree;
    git_oid blob_id, new_tree_id;

    git_tree* newest_tree;
    git_oid newest_try_id;

    //main_tree = get_root_tree(repo);
    create_blob(&blob_id, repo, file_content);
    ////
    git_tree_entry* tree_entry;
    int rc = git_tree_entry_bypath(&tree_entry, root_tree, path);
    if (rc == GIT_ENOTFOUND) {
        git_treebuilder* bld = NULL;
        error_check(git_treebuilder_new(&bld, repo, root_tree), "creating treebuilder");

        error_check(git_treebuilder_insert(&tree_entry, bld,
            filename,        /* filename */
            &blob_id, //          git_object_id(obj), /* OID */
            GIT_FILEMODE_BLOB) /* mode */
            , "file in dir");
        error_check(git_treebuilder_write(&new_tree_id, bld), "write builder to repo tree?");
        git_treebuilder_free(bld);

        git_tree_update updates[] = {
            { GIT_TREE_UPDATE_UPSERT, new_tree_id, GIT_FILEMODE_TREE, path}
        };
        error_check(git_tree_create_updated(&newest_try_id, repo, root_tree, 1, updates), "creating updated tree");

    }
    else {
        /* SLOW*/
        error_check(rc, "getting tree_entry by path");
        char* fullpath = join_paths(path, filename);
        git_tree_update updates[] = {
            { GIT_TREE_UPDATE_UPSERT, blob_id, GIT_FILEMODE_BLOB, fullpath}
        };
        error_check(git_tree_create_updated(&newest_try_id, repo, root_tree, 1, updates), "creating updated tree"); 
        free(fullpath);
        /**/

        // ALSO SLOW
        /*git_tree* sub_tree;
        git_tree_lookup(&sub_tree, repo, git_tree_entry_id(tree_entry));

        git_treebuilder* bld = NULL;
        error_check(git_treebuilder_new(&bld, repo, sub_tree), "creating treebuilder");
        error_check(git_treebuilder_insert(NULL, bld,
            filename,        // filename 
            &blob_id,       //  git_object_id(obj),
            GIT_FILEMODE_BLOB
        ), "file in dir");
        error_check(git_treebuilder_write(&new_tree_id, bld), "write builder to repo tree?");
        git_treebuilder_free(bld);

        bld = NULL;
        error_check(git_treebuilder_new(&bld, repo, root_tree), "creating treebuilder");
        error_check(git_treebuilder_insert(NULL, bld,
            path,        // filename 
            &new_tree_id,       //  git_object_id(obj),
            GIT_FILEMODE_TREE
        ), "file in dir");
        error_check(git_treebuilder_write(&newest_try_id, bld), "write builder to repo tree?");
        //git_tree_free(sub_tree);
        git_treebuilder_free(bld);
        */
        
    } 
    //git_tree_entry_free(tree_entry);

    /*git_tree_update updates[] = {
        { GIT_TREE_UPDATE_UPSERT, blob_id, GIT_FILEMODE_BLOB, filename}
    };*/
    

    //git_tree_free(root_tree);
    error_check(git_tree_lookup(&newest_tree, repo, &newest_try_id), "new tree lookup");

    return newest_tree;
}


static void create_commit(git_repository* repo, git_tree* root_tree, const char *directory, const char *msg, const char* filename, const char* file_content, bool is_initial) {
    git_signature* sig;
    git_oid commit_id;
    //git_tree* tree;
    git_commit* parent = NULL;
    int parent_count = 0;

    error_check(git_signature_now(&sig, "Me", "me@example.com"), "create signature");


    if (is_initial) {
         get_initial_tree(&root_tree, repo);
        //tree = get_tree_builder_tree(repo, "README.md", "this is a readme file", true);
    }else{
        parent_count = 1;
        parent = get_last_commit(repo);
        
        //using tree builder
        //tree = get_tree_builder_tree(repo, filename, file_content);

        //using GIT_TREE_UPDATE
        //tree = get_updated_tree(repo, filename, file_content);

        //tmp
        if (root_tree == NULL)
            get_tree_from_last_commit(&root_tree, repo);

        git_tree* new_tree;
        new_tree = get_updated_tree_with_dir(repo, root_tree, directory, filename, file_content);
        git_tree_free(root_tree);
        root_tree = new_tree;
        
    }

    error_check(git_commit_create_v(
        &commit_id, 
        repo, 
        "HEAD", 
        sig, 
        sig, 
        "UTF-8", 
        msg, 
        root_tree,
        parent_count, 
        parent
    ), "creating commit");

    git_signature_free(sig);
    //git_tree_free(tree);
    git_commit_free(parent);
}
int main(int argc, char *argv[]) {
    repo_params params = parse_params(argc, argv);
    print_all_params(&params);

    srand(clock());
    clock_t tic = clock();

    if(params.is_fastimport){
        run_fast_import(params.end, params.files_dir);
        clock_t toc = clock();
        printf("# Elapsed: %f seconds\n", (double)(toc - tic) / CLOCKS_PER_SEC);
        exit(0);
    }

    git_libgit2_init();

    git_repository* repo;
    repo = init_and_configure_repo(params.repo_path);
    git_tree* root_tree = NULL;
    //---------------
    if (params.is_initial) {
        create_commit(repo, root_tree, params.files_dir, "initial commit", NULL, NULL, true); //no file is added
    }
    for (int i = params.start; i < params.end; i++) {
        char* filename = get_filename_by_index(i);
        char* content = get_random_string(5000, 1);
        create_commit(repo, root_tree, params.files_dir, "commit", filename, content, false);
        free(filename);              
    }
    //---------------
    git_tree_free(root_tree);
    git_repository_free(repo);

    clock_t toc = clock();
    printf("Elapsed: %f seconds\n", (double)(toc - tic) / CLOCKS_PER_SEC);

    return 0;
}