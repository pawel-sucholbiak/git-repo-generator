#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "RepoParams.h"

repo_params parse_params(int argc, char **argv) {
	repo_params params = {
            .start = 0, .end = 2
    };

	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "--start") == 0) {
			if (i + 1 >= argc) {
				printf("[WARN] missing value for 'start' param\n");
				continue;
			}
			params.start = atoi(argv[++i]);
			continue;
		}
		if (strcmp(argv[i], "--end") == 0) {
			if (i + 1 >= argc) {
				printf("[WARN] missing value for 'end' param\n");
				continue;
			}
			params.end = atoi(argv[++i]);
			continue;
		}
		printf("[WARN] unknown param or wrong parsing - %s\n", argv[i]);
	}

	return params;
}
void print_all_params(repo_params* params) {
	printf("#parsed params:\n#\tstart: %d\n#\tend: %d\n",
		params->start,
		params->end
	);
}