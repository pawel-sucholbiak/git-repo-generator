#pragma once

typedef struct repo_params {
	int start;           
	int end;
} repo_params;

repo_params parse_params(int argc, char** argv);
void print_all_params(repo_params* params);