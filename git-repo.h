#pragma once

#include <git2.h>

#include <stdbool.h>
void error_check(int error) {
    if (error < 0) {
        const git_error* e = giterr_last();
        printf("Error %d/%d: %s\n", error, e->klass, e->message);
        exit(error);
    }
}

void create_blob(git_repository* repo, git_oid* blob_oid, const char str[]) {
    //    int error = git_blob_create_fromworkdir(&oid, repo, "README.md");
    //    error = git_blob_create_fromdisk(&oid, repo, "/etc/hosts");

    //    const char str[] = "# Hello there!";
    int error = git_blob_create_frombuffer(blob_oid, repo, str, strlen(str));
    error_check(error);

    return;
}

void create_tree_with_blob(git_repository* repo, git_oid* out_tree_id, const git_oid* blob_oid, const git_tree* source_tree, const char file[]) {
    git_treebuilder* bld = NULL;
    int error;
    if (source_tree != NULL) {
        error = git_treebuilder_new(&bld, repo, source_tree);
    }
    else {
        error = git_treebuilder_new(&bld, repo, NULL);
    }
    error_check(error);

    /* Add some entries */
    //    git_object *obj = NULL;
    //    error = git_revparse_single(&obj, repo, "HEAD:README.md");
    error = git_treebuilder_insert(NULL, bld,
        file,        /* filename */
        blob_oid, /* OID */
        GIT_FILEMODE_BLOB); /* mode */
    error_check(error);
    //    git_object_free(obj);
    //    error = git_revparse_single(&obj, repo, "v0.1.0:foo/bar/baz.c");
    //    error = git_treebuilder_insert(NULL, bld,
    //                                   "d.c",
    //                                   git_object_id(obj),
    //                                   GIT_FILEMODE_BLOB);
    //    git_object_free(obj);

    error = git_treebuilder_write(out_tree_id, bld);
    error_check(error);

    git_treebuilder_free(bld);
}
void lookup(git_repository* repo, git_oid* oid,
    git_commit** commit,
    git_tree** tree,
    git_blob** blob,
    git_tag** tag,
    int type//1: commit, 2: tree, 3: blob, 4: tag
) {
    int error;
    if (type == 1) {
        error = git_commit_lookup(commit, repo, oid);
        error_check(error);
    }
    else if (type == 2) {
        error = git_tree_lookup(tree, repo, oid);
        error_check(error);
    }
    else if (type == 3) {
        error = git_blob_lookup(blob, repo, oid);
        error_check(error);
    }
    else if (type == 4) {
        error = git_tag_lookup(tag, repo, oid);
        error_check(error);
    }

    return;
}
void create_init_commit(git_repository* repo, git_oid* out_new_commit_id, const git_tree* tree, const char message[]) {

    // commit repo
    git_signature* me = NULL;
    int error = git_signature_now(&me, "Me", "me@example.com");
    error_check(error);

    error = git_commit_create_v(
        out_new_commit_id,
        repo,
        "HEAD",                      /* name of ref to update */
        me,                          /* author */
        me,                          /* committer */
        "UTF-8",                     /* message encoding */
        message,  /* message */
        tree,                        /* root tree */
        0,                           /* parent count */
        NULL);                    /* parents */
    error_check(error);

    return;
}
git_commit* get_last_commit(git_repository* repo)
{
    int rc;
    git_commit* commit = NULL; /* the result */
    git_oid oid_parent_commit;  /* the SHA1 for last commit */

    /* resolve HEAD into a SHA1 */
    rc = git_reference_name_to_id(&oid_parent_commit, repo, "HEAD");
    if (rc == 0)
    {
        /* get the actual commit structure */
        rc = git_commit_lookup(&commit, repo, &oid_parent_commit);
        if (rc == 0)
        {
            return commit;
        }
    }
    return NULL;
}

git_tree* main_init_commit(git_repository* repo) {
    //create blob, tree and commit
        //create blob
    git_oid blob_oid;
    create_blob(repo, &blob_oid, "init commit. Hello there!");
    //create tree
    git_oid tree_oid;
    create_tree_with_blob(repo, &tree_oid, &blob_oid, NULL, "create_file.txt");
    // lookup tree struct
    git_tree* new_tree;
    //lookup(repo, &tree_oid,NULL, &new_tree, NULL, NULL, 2);
    int tree_lookup_error = git_tree_lookup(&new_tree, repo, &tree_oid);
    error_check(tree_lookup_error);
    //create commit
    git_oid commit_oid;
    create_init_commit(repo, &commit_oid, new_tree, "init commit!");


    printf("init commit completed!\n");

    return new_tree;
}

void add_new_commit(git_repository* repo, git_tree* main_tree) {
    git_oid file_content_id;

    create_blob(repo, &file_content_id, "Some random text");

    git_oid* new_tree_id;

    git_tree_update updates[] = {
        { GIT_TREE_UPDATE_UPSERT, file_content_id, GIT_FILEMODE_BLOB, "test.txt"}
    };

    int tree_updated_error = git_tree_create_updated(&new_tree_id, repo, main_tree, 1, updates);
    error_check(tree_updated_error);

    git_tree* new_tree;
    int tree_lookup_error = git_tree_lookup(&new_tree, repo, &new_tree_id);
    error_check(tree_lookup_error);

    git_signature* me;
    int sig_error = git_signature_now(&me, "Me", "me@example.com");
    error_check(sig_error);

    git_oid* new_commit_id = NULL;

    git_commit* parent_commit = get_last_commit(repo);

    // --- test ---
    //git_repository *git_repo_owner = git_tree_owner(new_tree);
    // --- test ---
//https://stackoverflow.com/questions/54195446/how-to-commit-from-the-given-files

    //repo owner is different than updated tree owner
    int commit_error = git_commit_create_v(
        &new_commit_id,
        repo,
        "HEAD",                      /* name of ref to update */
        me,                          /* author */
        me,                          /* committer */
        "UTF-8",                     /* message encoding */
        "commit messsge no2",  /* message */
        new_tree,                        /* root tree */
        1,                           /* parent count */
        parent_commit);                    /* parents */
    error_check(commit_error);

}


bool addGitCommit(git_repository* repo, git_signature* sign, const char* content, int content_sz, const char* message)
{
    git_oid oid_blob;    /* the SHA1 for our blob in the tree */
    git_oid oid_tree;    /* the SHA1 for our tree in the commit */
    git_oid oid_commit;  /* the SHA1 for our initial commit */
    git_blob* blob;     /* our blob in the tree */
    git_tree* tree_cmt; /* our tree in the commit */
    git_treebuilder* tree_bld;  /* tree builder */
    bool b = false;

    /* create a blob from our buffer */
    git_blob_create_frombuffer(
        &oid_blob,
        repo,
        content,
        content_sz);
    /* blob created */
    
    git_blob_lookup(&blob, repo, &oid_blob);
    /* blob created and found */
    
    git_treebuilder_create(&tree_bld, NULL);
    
    /* a new tree builder created */
    
    git_treebuilder_insert(
                    NULL,
                    tree_bld,
                    "name-of-the-file.txt",
                    &oid_blob,
                    GIT_FILEMODE_BLOB);
    
    /* blob inserted in tree */
    git_treebuilder_write(
                        &oid_tree,
                        repo,
                        tree_bld);
    
    /* the tree was written to the database */
    
    git_tree_lookup(
                            &tree_cmt, repo, &oid_tree);
    /*we've got the tree pointer */
    
    git_commit_create(
                                &oid_commit, repo, "HEAD",
                                sign, sign, /* same author and commiter */
                                NULL, /* default UTF-8 encoding */
                                message,
                                tree_cmt, 0, NULL);
    
    git_tree_free(tree_cmt);
                git_treebuilder_free(tree_bld);
            git_blob_free(blob);
    return b;
}