#include <stdio.h>
#include <time.h>

#include "FileHandler.h"
#include "RepoParams.h"
#include "FastExport.h"

int main(int argc, char *argv[]) {
    repo_params params = parse_params(argc, argv);
    print_all_params(&params);

    srand(clock());
    clock_t tic = clock();

    run_fast_export(params.end);
    clock_t toc = clock();
    printf("# Elapsed: %f seconds\n", (double) (toc - tic) / CLOCKS_PER_SEC);
    exit(0);
}
